package com.SocialMediaAggregator.controller;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.SocialMediaAggregator.model.Interest;
import com.SocialMediaAggregator.model.Tweet;
import com.SocialMediaAggregator.service.AppUserServiceImp;
import com.SocialMediaAggregator.service.InterestServiceImp;

import twitter4j.TwitterException;
import twitter4j.User;

@RestController
@RequestMapping("/smapp/interests")	
public class InterestController {
	@Autowired
    AppUserServiceImp appUserService;
	@Autowired
    InterestServiceImp interestService;

	@RequestMapping(value = "/tweets", method = RequestMethod.POST)
	public List<Tweet> searchTweetsOfAllInterests(@RequestBody Set<Interest> interests) throws TwitterException {
		return interestService.getTweets(interests);
	}	
	
	@RequestMapping(value = "/{interestId}/info", method = RequestMethod.GET)
	public User getInfo(@PathVariable Long interestId) throws TwitterException {
		return interestService.getInterestInfo(interestId);
	}
	
}
