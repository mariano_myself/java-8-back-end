package com.SocialMediaAggregator.controller;

import java.util.Set;

import javax.servlet.ServletException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.SocialMediaAggregator.model.AppUser;
import com.SocialMediaAggregator.model.Interest;
import com.SocialMediaAggregator.service.AppUserServiceImp;
import com.SocialMediaAggregator.service.InterestServiceImp;

@RestController
@RequestMapping("/smapp/user/{userId}")
public class AppUserController {
	@Autowired
    AppUserServiceImp appUserService;
	@Autowired
    InterestServiceImp interestService;
    
	@RequestMapping(method = RequestMethod.GET)
	public AppUser getUserByName(@PathVariable Long userId) {
		return appUserService.findById(userId);
	}
	
    @RequestMapping(value = "/interests", method = RequestMethod.GET)
    public Set<Interest> getInterests(@PathVariable Long userId) {
    	Set<Interest> interests = appUserService.findById(userId).getInterests();
        return interests;
    }
    
	@RequestMapping(value = "/interest", method = RequestMethod.POST)
    public Interest followInterest(@PathVariable Long userId, @RequestBody Interest interest) throws ServletException {
		return appUserService.addInterest(userId, interest);
    }

	@RequestMapping(value = "/interest/{interestId}", method = RequestMethod.DELETE)
    public void unfollowInterest(@PathVariable Long userId, @PathVariable Long interestId) {
		appUserService.deleteInterest(userId, interestId);
    }	

}
