package com.SocialMediaAggregator.controller;

import javax.servlet.ServletException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.SocialMediaAggregator.model.AppUser;
import com.SocialMediaAggregator.model.LoginResponse;
import com.SocialMediaAggregator.service.AppUserServiceImp;
import com.SocialMediaAggregator.service.LoginServiceImp;

@RestController
@RequestMapping("/smapp")
public class LoginController {
	@Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;
	@Autowired
    AppUserServiceImp appUserService;
	@Autowired
    LoginServiceImp loginService;
    
	@RequestMapping(value = "/sign-up", method = RequestMethod.POST)
    public void signUp(@RequestBody AppUser user) throws ServletException {
		if (appUserService.findByUsername(user.getUsername()) != null) {
			throw new ServletException("Username already in use");
		}
		else {
	        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
	        appUserService.save(user);
		}
    }
	
	@RequestMapping(value = "/login", method = RequestMethod.POST)
    public LoginResponse login(@RequestBody AppUser user) throws ServletException {
		LoginResponse loginResponse = new LoginResponse();
		loginResponse.setUser(appUserService.findByUsername(user.getUsername()));
		loginResponse.setToken(loginService.getToken(user));
		return loginResponse;
    }

}
