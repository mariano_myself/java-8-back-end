package com.SocialMediaAggregator.service;

import static com.SocialMediaAggregator.security.SecurityConstants.EXPIRATION_TIME;
import static com.SocialMediaAggregator.security.SecurityConstants.SECRET;

import java.util.Date;

import javax.servlet.ServletException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.SocialMediaAggregator.model.AppUser;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Service
public class LoginServiceImp implements LoginService {
	@Autowired
    AppUserServiceImp appUserService;
	@Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;
	
	@Override
	public String getToken(AppUser user) throws ServletException {
	    if (user.getUsername() == null || user.getPassword() == null) {
	        throw new ServletException("Please fill in username and password.");
	    }

	    String username = user.getUsername();
	    String password = user.getPassword();

	    AppUser userFound = appUserService.findByUsername(username);

	    if (userFound == null) {
	        throw new ServletException("Username not found.");
	    }

	    if (!bCryptPasswordEncoder.matches(password, userFound.getPassword())) {
	        throw new ServletException("Invalid login. Please check your username and password.");
	    }

	    
	    String jwtToken = Jwts.builder()
	    		.setSubject(user.getUsername())
	            .setExpiration(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
	            .signWith(SignatureAlgorithm.HS512, SECRET)
	            .compact();

	    return jwtToken;
	}

}
