package com.SocialMediaAggregator.service;

import java.util.HashSet;
import java.util.List;

import javax.servlet.ServletException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.SocialMediaAggregator.model.AppUser;
import com.SocialMediaAggregator.model.Interest;
import com.SocialMediaAggregator.repository.AppUserRepository;

@Service
public class AppUserServiceImp implements AppUserService{
    
	@Autowired
    private AppUserRepository repository;
	@Autowired
    InterestServiceImp interestService;

    @Override
    public List<AppUser> findAll() {
        List<AppUser> appUsers = (List<AppUser>) repository.findAll();     
        return appUsers;
    }
    
    @Override
    public void save(AppUser user) {
    	repository.save(user);
    }

	@Override
	public AppUser findByUsername(String username) {
		AppUser appUser = repository.findByUsername(username);
		return appUser;
	}

	@Override
	public AppUser findById(Long id) {
		AppUser appUser = repository.findById(id).get();
		return appUser;
	}

	@Override
	public Interest addInterest(Long userId, Interest interest) throws ServletException {
		AppUser user = this.findById(userId);
		if (user.getInterests().contains(interest)) throw new ServletException("You already have this interest");
		Interest interestCreated = interestService.findByName(interest.getName());
		if (interestCreated != null) {
			interestCreated.getUsers().add(user);
			interestCreated = interestService.save(interestCreated);
		}
		else {
			HashSet<AppUser> users = new HashSet<AppUser>();
			users.add(user);
			interest.setName(interest.getName().toLowerCase());
			interest.setUsers(users);
			interestCreated = interestService.save(interest);
		}
		user.getInterests().add(interestCreated);
        this.save(user);
        return interestCreated;
	}

	@Override
	public void deleteInterest(Long userId, Long interestId) {
		AppUser user = this.findById(userId);
		Interest interestFounded = interestService.findById(interestId);
		user.getInterests().remove(interestFounded);
        this.save(user);
	}
}
