package com.SocialMediaAggregator.service;

import javax.servlet.ServletException;

import com.SocialMediaAggregator.model.AppUser;

public interface LoginService {
	public String getToken(AppUser user) throws ServletException;
}
