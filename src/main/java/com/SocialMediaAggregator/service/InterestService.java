package com.SocialMediaAggregator.service;

import java.util.List;
import java.util.Set;

import com.SocialMediaAggregator.model.Interest;
import com.SocialMediaAggregator.model.Tweet;

import twitter4j.TwitterException;
import twitter4j.User;

public interface InterestService {
	public List<Interest> findAll();
	public Interest save(Interest interest);
	public Interest findById(Long id);
	public Interest findByName(String name);
	public List<Tweet> getTweets(Set<Interest> interests) throws TwitterException;
	public User getInterestInfo(Long interestId) throws TwitterException;
}
