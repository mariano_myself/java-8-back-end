package com.SocialMediaAggregator.service;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.SocialMediaAggregator.model.Interest;
import com.SocialMediaAggregator.model.Tweet;
import com.SocialMediaAggregator.repository.InterestRepository;

import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.User;

@Service
public class InterestServiceImp implements InterestService{
    
	@Autowired
    private InterestRepository repository;

	@Override
	public Interest findById(Long id) {
		Interest interest = repository.findById(id).get();
		return interest;
	}
	
    @Override
    public List<Interest> findAll() {
        List<Interest> interests = (List<Interest>) repository.findAll();
        return interests;
    }
    
    @Override
    public Interest save(Interest interest) {
    	return repository.save(interest);
    }

	@Override
	public Interest findByName(String name) {
		Interest interest = repository.findByName(name);
		return interest;
	}

	private String getTweetsSearchQuery(Set<Interest> interests) {
		String query = "";
		for (Interest interest : interests) {
			query = query.concat(interest.getPrefijo()).concat(interest.getName()).concat(" ");
		}
		return query;
	}	
	
	@Override
	public List<Tweet> getTweets(Set<Interest> interests) throws TwitterException {
		TwitterFactory tf = new TwitterFactory();
		Twitter twitter = tf.getInstance();
		Query query4j = new Query(this.getTweetsSearchQuery(interests));
		query4j.setCount(100);
	    QueryResult result = twitter.search(query4j);
	    
	    return result.getTweets().stream()
	  	      .map(item -> {
	  	    	  Tweet tweet = new Tweet();
	  	    	  tweet.setText(item.getText());
	  	    	  tweet.setAuthor("@".concat(item.getUser().getScreenName()));
	  	    	  tweet.setCreatedAt(item.getCreatedAt());
	  	    	  return tweet;
	  	      }).collect(Collectors.toList());
	}

	@Override
	public User getInterestInfo(Long interestId) throws TwitterException {
		TwitterFactory tf = new TwitterFactory();
		Twitter twitter = tf.getInstance();
	    return twitter.users().showUser(this.findById(interestId).getName());
	}	
	
}
