package com.SocialMediaAggregator.service;

import java.util.List;

import javax.servlet.ServletException;

import com.SocialMediaAggregator.model.AppUser;
import com.SocialMediaAggregator.model.Interest;

public interface AppUserService {
	public List<AppUser> findAll();
	public void save(AppUser user);
	public AppUser findByUsername(String username);
	public AppUser findById(Long id);
	public Interest addInterest(Long userId, Interest interest) throws ServletException;
	public void deleteInterest(Long userId, Long interestId);
}
