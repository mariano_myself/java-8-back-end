package com.SocialMediaAggregator.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "APP_USER")
public class AppUser implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public AppUser() {
		 
	};
 
	public AppUser(String username) {
		this.username = username;
 
	};
	
	public AppUser(String username, String password) {
		this.username = username;
		this.password = password;
	};
 
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(unique = true)
	private String username;
	private String password;
	private String name;
	private String lastname;
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@ManyToMany(cascade = {CascadeType.ALL})
	@JoinTable(name="UserInterst", joinColumns={@JoinColumn(name="IdUser")}, inverseJoinColumns={@JoinColumn(name="IdInterest")})
	private Set<Interest> interests = new HashSet<Interest>();
	@ManyToMany(cascade = {CascadeType.ALL})
	@JoinTable(name="UserUserFollowed", joinColumns={@JoinColumn(name="IdUser")}, inverseJoinColumns={@JoinColumn(name="IdUserFollowed")})
	private Set<Interest> usersFollowed = new HashSet<Interest>();
 
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
 
	public String getUsername() {
		return username;
	}
 
	public void setUsername(String username) {
		this.username = username;
	}
	 
	public Set<Interest> getInterests() {
		return interests;
	}

	public Set<Interest> getUsersFollowed() {
		return usersFollowed;
	}

	public void setUsersFollowed(Set<Interest> usersFollowed) {
		this.usersFollowed = usersFollowed;
	}

	public void setInterests(Set<Interest> interests) {
		this.interests = interests;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	
}
