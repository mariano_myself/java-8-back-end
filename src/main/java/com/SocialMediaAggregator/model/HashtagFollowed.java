package com.SocialMediaAggregator.model;

import java.util.HashSet;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("Hashtag")
public class HashtagFollowed extends Interest {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4476828028827418760L;
	public HashtagFollowed() {
		 
	};
 
	public HashtagFollowed(String name) {
		this.name = name;
	};

	public HashtagFollowed(String name, HashSet<AppUser> users) {
		this.name = name;
		this.users = users;
		this.prefijo = "#";
	};
	
	@Override
	public String getPrefijo() {
		return "#";
	}
	
	@Override
	public String getType() {
		return "Hashtag";
	}
}
