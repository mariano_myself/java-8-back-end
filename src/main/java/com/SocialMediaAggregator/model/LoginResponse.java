package com.SocialMediaAggregator.model;

public class LoginResponse {
	private String token;
	private AppUser user;
	
	public LoginResponse() {
		
	}
	
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public AppUser getUser() {
		return user;
	}
	public void setUser(AppUser user) {
		this.user = user;
	}
}
