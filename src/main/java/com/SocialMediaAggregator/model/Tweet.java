package com.SocialMediaAggregator.model;

import java.util.Date;

public class Tweet {
	private String text;
	private String author;
	private Date createdAt;
	
	public Tweet() {
		
	};
	
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public Date getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	
	
}
