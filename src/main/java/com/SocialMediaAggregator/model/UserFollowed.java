package com.SocialMediaAggregator.model;

import java.util.HashSet;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("User")
public class UserFollowed extends Interest{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2358909534451629189L;
	public UserFollowed() {
		 
	};
 
	public UserFollowed(String name) {
		this.name = name;
	};

	public UserFollowed(String name, HashSet<AppUser> users) {
		this.name = name;
		this.users = users;
		this.prefijo = "@";
	};
	
	@Override
	public String getPrefijo() {
		return "@";
	}
	
	@Override
	public String getType() {
		return "User";
	}
}
