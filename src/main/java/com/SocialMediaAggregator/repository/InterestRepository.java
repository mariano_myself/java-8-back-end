package com.SocialMediaAggregator.repository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.SocialMediaAggregator.model.Interest;

@Repository
public interface InterestRepository extends CrudRepository<Interest, Long>{
	Interest findByName(String name);
}
