# Social Media Aggregator for Twitter

Respositorio que contiene el código fuente del Back-End de una aplicación "Social Media Aggregator", la cual se conecta con Twitter.


## Arquitectura

1. Java
2. Spring Boot
3. Spring Security
4. Hibernate
5. Twitter4J API
6. Postgresql

## Puesta en marcha

1. Configurar su base de datos local, en este caso se esta utilizando Postgresql.
2. Dentro del archivo application.properties cambiar los datos del Datasource.
3. Crear una cuenta Dev en Twitter y crear una app para obtener el Token de acceso: https://apps.twitter.com/
4. Dentro del archivo twitter4j.properties cambiar los datos del Token de acceso obtenido.
4. En la linea de comandos poner los siguientes comandos:
	1. mvn clean install
	2. mvn eclipse:eclipse (Para poder abrir el proyecto desde el IDE Eclipse)
	3. mvn spring-boot:run
	4. Con un cliente Rest invocar a las distintas URLs para lo que se necesite 

## Servicios REST expuestos

1. API para comenzar a seguir (y dejar de hacerlo) un tema de interés o un usuario. Los intereses se identifican con un “#”, y los usuarios se identifican con “@”. Por ejemplo, si los intereses de un usuario son #River, @liomessi y #Boca es porque quiere visualizar los posts públicos que mencionan los temas #River o #Boca, y todos los posts sociales del usuario @leomessi (independientemente de si mencionan o no a los dos temas anteriores).
2. API para obtener todos los intereses, o un interes particular.
3. API para obtener todos los tweets de todos los intereses, o los tweets de un interés particular.

